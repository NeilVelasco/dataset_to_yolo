import os
import argparse
import cv2
from xml.dom import minidom

class XmlReader():
    def __init__(self):
        self.frame_list = []
        self.save_list = []

    def create_train_txt(self):
        f_train = open('data/train.txt', 'w+')
        file_dir = 'data/obj_train_data/'
        for items in self.save_list:
            f_train.write(file_dir + items + "\n")
        f_train.close()

    def read_xml(self, xml_file):
        xmldoc = minidom.parse(xml_file)
        boxlist = xmldoc.getElementsByTagName('box')
        im_width = float(xmldoc.getElementsByTagName('width')[0].childNodes[0].nodeValue)
        im_height = float(xmldoc.getElementsByTagName('height')[0].childNodes[0].nodeValue)
        print("Reading xml")

        for box in boxlist:
            if box.attributes['outside'].value == '0':
                frame = int(box.attributes['frame'].value)
                self.frame_list.append(frame)
                xtl = float(box.attributes['xtl'].value)
                ytl = float(box.attributes['ytl'].value)
                xbr = float(box.attributes['xbr'].value)
                ybr = float(box.attributes['ybr'].value)
                
                xc = ((xbr + xtl) / 2) / im_width
                yc = ((ybr + ytl) / 2) / im_height
                w = (xbr - xtl) / im_width
                h = (ybr - ytl) / im_height

                file_num = str(frame).zfill(6)
                file_name = 'data/obj_train_data/frame_' + file_num + '.txt'
                print("Opening file", file_name)
                write_file = open(file_name, 'a+')
                write_file.write(f"0 {xc:.6f} {yc:.6f} {w:.6f} {h:.6f}\n")
                print('Writing ', file_name)
            else:
                continue

    def read_vid(self, vid_name):
        vid = cv2.VideoCapture(vid_name)
        frame_num = 0
        while True:
            ret, frame = vid.read()
            if not ret:
                print('Video parsing ended')
                break
            if not frame_num in self.frame_list:
                print('Skipping frame %s', frame_num)
                frame_num += 1
                continue
            
            save_name = 'frame_' + str(frame_num).zfill(6) + '.PNG'
            self.save_list.append(save_name)
            cv2.imwrite('data/obj_train_data/' + save_name, frame)
            print('Writing', save_name)
            frame_num += 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-x', '--xml', type=str, default='annotations.xml')
    parser.add_argument('-v', '--vid', type=str, default='video.mp4')
    args = parser.parse_args()
    
    reader = XmlReader()
    if not os.path.isdir("data"):
        os.makedirs("data")
    if not os.path.isdir("data/obj_train_data"):
        os.makedirs("data/obj_train_data")
        print("Created folder!")
    reader.read_xml(args.xml)
    reader.read_vid(args.vid)
    reader.create_train_txt()

    print("Done!")


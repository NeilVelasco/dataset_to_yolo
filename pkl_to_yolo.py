import pickle
import os
import argparse

'''
pkl contents from the dict:
    filename: 
    videoname:
    frameid:
    width:
    height:
    ann: {
        bboxes:
        labels:
        trackids:
        bboxes_ignore:
        labels_ignore:
        trackids_ignore:
    }
'''

class PklReader():
    def __init__(self, train_pkl, test_pkl):
        self.train_pkl = train_pkl
        self.test_pkl = test_pkl

    def create_train_txt(self):
            f_train = open('train.txt', 'w+')
            file_dir = 'data/train/'
            ann_dir = 'train_ann'
            files_ann_list = os.listdir(ann_dir)
            files_ann_list.sort()
            for items in files_ann_list:
                items = items.replace('.txt', '.jpg')
                f_train.write(file_dir + items + "\n")
            f_train.close()

    def create_test_txt(self):
            f_test = open('test.txt', 'w+')
            file_dir = 'data/test/'
            ann_dir = 'test_ann'
            files_ann_list = os.listdir(ann_dir)
            files_ann_list.sort()
            for items in files_ann_list:
                items = items.replace('.txt', '.jpg')
                f_test.write(file_dir + items + "\n")
            f_test.close()

    def train_ann(self):
        list_labels = []
        with open(self.train_pkl, 'rb') as f:
            data = pickle.load(f)
            print(len(data))
            for i in range(0, len(data)):
                filename = data[i]['filename']
                ann = data[i]['ann']
                filename = filename.replace('.jpg', '.txt')
                width = data[i]['width']
                height = data[i]['height']
                ann_file = open('train_ann/' + filename, 'w+')

                bboxes = ann['bboxes']
                labels = ann['labels']
                bboxes_ignore = ann['bboxes_ignore']
                labels_ignore = ann['labels_ignore']
                trackids_ignore = ann['trackids_ignore']
                print(labels)

                if len(bboxes_ignore) > 0 or len(labels_ignore) > 0 or len(trackids_ignore) > 0:
                    print('There is an ignore')
                    print('Ignores are not yet implemented in this pkl converter')
                    print('Please contact the developer')
                    print(bboxes_ignore)
                    print(labels_ignore)
                    print(trackids_ignore)
                    break

                for j in range(0, len(bboxes)):
                    xtl = float(bboxes[j][0])
                    ytl = float(bboxes[j][1])
                    xbr = float(bboxes[j][2])
                    ybr = float(bboxes[j][3])
                    # print(xtl, ytl, xbr, ybr)

                    xc = ((xbr + xtl) / 2) / width
                    yc = ((ybr + ytl) / 2 ) / height
                    w = (xbr - xtl) / width
                    h = (ybr - ytl) / height
                    
                    list_labels.append(labels[j])
                    # print(xc, yc, w, h)
                    #ann_file.write(f"{labels[j]} {xc:.6f} {yc:.6f} {w:.6f} {h:.6f}\n")

            print("Max label num:", max(list_labels), "\n")

    def test_ann(self):
        with open(self.test_pkl, 'rb') as f:
            data = pickle.load(f)
            print(len(data))
            for i in range(0, len(data)):
                filename = data[i]['filename']
                ann = data[i]['ann']
                filename = filename.replace('.jpg', '.txt')
                width = data[i]['width']
                height = data[i]['height']
                ann_file = open('test_ann/' + filename, 'w+')

                bboxes = ann['bboxes']
                labels = ann['labels']
                bboxes_ignore = ann['bboxes_ignore']
                labels_ignore = ann['labels_ignore']
                trackids_ignore = ann['trackids_ignore']

                if len(bboxes_ignore) > 0 or len(labels_ignore) > 0 or len(trackids_ignore) > 0:
                    print('There is an ignore')
                    print('Ignores are not yet implemented in this pkl converter')
                    print('Please contact the developer')
                    print(bboxes_ignore)
                    print(labels_ignore)
                    print(trackids_ignore)
                    break

                for j in range(0, len(bboxes)):
                    xtl = float(bboxes[j][0])
                    ytl = float(bboxes[j][1])
                    xbr = float(bboxes[j][2])
                    ybr = float(bboxes[j][3])
                    # print(xtl, ytl, xbr, ybr)

                    xc = ((xbr + xtl) / 2) / width
                    yc = ((ybr + ytl) / 2 ) / height
                    w = (xbr - xtl) / width
                    h = (ybr - ytl) / height

                    # print(xc, yc, w, h)
                    ann_file.write(f"{labels[j]} {xc:.6f} {yc:.6f} {w:.6f} {h:.6f}\n")
        

if __name__ == '__main__':
    if not os.path.isdir("train"):
        os.makedirs("train")
    if not os.path.isdir("test"):
        os.makedirs("test")
        print("Created folders!")
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', type=str, default="train_combined.pkl")
    parser.add_argument('--test', type=str, default="test_combined.pkl")
    args = parser.parse_args()

    reader = PklReader(args.train, args.test)
    reader.train_ann()
    reader.create_train_txt()
    reader.test_ann()
    reader.create_test_txt()
    print("Finished!")


